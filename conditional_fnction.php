<?php
session_start();

function showchatwindow(){
    echo "im inside:".__FUNCTION__;
 //chat form
}
function showloginbox(){
    echo "im inside:".__FUNCTION__;
 //chat form
}
if(isset($_SESSION["c8nick"])){
    echo $_SESSION["c8nick"];
    showchatwindow();
}
else{
    echo showloginbox();
}
echo "<br>";

//else and if only
$situation = true;
if($situation == true){
    echo "the situation is true";
}
else{
    echo "situation is false";
}
echo "<br>";

//if else use
$x = 34;
$y = 76;
if($x>$y){
    echo "x is greater than y";
}
else if($x<$y){
    echo "y is greater than x";
}
else{
    echo "x and y equal";
}
echo "<br>";

// if and elseif used
$x = 23;
$y = 54;
$z = 15;

if(($x>$y) && ($x>$z)){
    echo "x is greatest:";
}
elseif(($y>$x) && ($y>$z)){
    echo "y is greatest:";
}
elseif(($z>$x) && ($z>$y)){
    echo "z is greatest:";
}
else{

}
echo "<br>";

if(($x<$y) && ($x<$z)){
    echo "x is smallest:";
}
elseif(($y<$x) && ($y<$z)){
    echo "y is smallest:";
}
elseif(($z<$x) && ($z<$y)){
    echo "z is smallest:";
}
else{

}
echo "<br>";
$a = 67;
switch($a){
    case ($a>=80):
        echo "the result is A+";
        break;
    case ($a>=70 && $a<80):
        echo "the result is A";
        break;
    case ($a>=60 && $a<70):
        echo "the result is A-";
        break;
    case ($a>=50 && $a<60):
        echo "the result is B";
        break;
    case ($a>=40 && $a<50):
        echo "the result is C";
        break;
    case ($a>=33 && $a<40):
        echo "the result is D";
        break;
    default:echo "fail";
}