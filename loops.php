<?php
echo "php-";
$i = 1;
while($i<=44) {       //here 1 use for infinite loop
    echo "B$i";
    if($i < 44) {
        echo ",";
    }
    else {
            echo ".";
        }
    $i++;
}
//end of while loop

//dowhile

echo "<br>";
$i = 11;
do{
    echo "hellow $i <br>";
    $i++;

}while($i<=10);

//for loop

for($i=1 ;$i<=10 ;$i++)
{
    echo "hi $i";

}

//nested loop
echo "<br>";
$myarray = array();
$count = 1;
echo "<table border='2'>";
for($i=0;$i<=3;$i++){
    echo "<tr>";
   for($j=0;$j<=3;$j++){

       $myarray[$i][$j] = $count;
       echo "<td>";
       print_r($myarray[$i][$j]);
       echo "</td>";
       $count++;
   }
    echo "<tr>";
}
echo "</table>";

//1, 1 2, 1 2 3
echo "<br>";
$myarray = array();
$count = 1;
echo "<table border='2'>";
for($i=0;$i<=3;$i++){
    echo "<tr>";
    for($j=0;$j<=$i;$j++){
        $myarray[$i][$j] = $count;
        echo "<td>";
        print_r($myarray[$i][$j]);
        echo "</td>";
        $count++;
    }
    echo "<tr>";
}
echo "</table>";

//myarray
echo "<br>";

$myarray =array("afroja"=>22,"kuddus"=>32,"moynar ma"=>43);
foreach($myarray as $key=>$value){
    echo "myarray['$key'] is [$value]<br>";
}

?>


